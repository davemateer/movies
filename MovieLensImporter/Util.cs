﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using MiniProfiler.Integrations;
using StackExchange.Profiling.Data;
using StackExchange.Profiling.SqlFormatters;

namespace MovieLensImporter
{
    public static class Util
    {
        // Appveyor is configured to set AppveyorSQL2014ConnectionString in appveyor.yml
        // Otherwise get the value from web.config (BookTech) in MVC project
        // or app.config (BookTechTesting) in IntegrationTests project
        //static readonly string ConnectionString =
        //    Environment.GetEnvironmentVariable("AppveyorSQL2014ConnectionString") ??
        //    ConfigurationManager.ConnectionStrings["DMoviesConnectionString"].ConnectionString;
        static string ConnectionString = 
            ConfigurationManager.ConnectionStrings["DMoviesConnectionString"].ConnectionString;

        public static IDbConnection GetOpenConnection()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            return connection;

            //MiniProfiler.Settings.SqlFormatter = new SqlServerFormatter();
            //return new ProfiledDbConnection(connection, CustomDbProfiler.Current);
        }
    }
}
