﻿using Amazon.PAAPI.WCF;
using Dapper;
using MoviesWeb.Amazon.PAAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using CsvHelper;
using MiniProfiler.Integrations;

namespace MovieLensImporter
{
    // What this app uses as the concept of a movie
    class Movie
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public int MovieLensMovieID { get; set; }
        public int TMDBID { get; set; }
        public int IMDBID { get; set; }
    }

    // TMDB Movie 
    public class TMDBMovieResponse
    {
        public bool adult { get; set; }
        public string backdrop_path { get; set; }
        public object belongs_to_collection { get; set; }
        public int budget { get; set; }
        public Genre[] genres { get; set; }
        public string homepage { get; set; }
        public int id { get; set; }
        public string imdb_id { get; set; }
        public string original_language { get; set; }
        public string original_title { get; set; }
        public string overview { get; set; }
        public float popularity { get; set; }
        public string poster_path { get; set; }
        public Production_Companies[] production_companies { get; set; }
        public Production_Countries[] production_countries { get; set; }
        public string release_date { get; set; }
        public int revenue { get; set; }
        public int runtime { get; set; }
        public Spoken_Languages[] spoken_languages { get; set; }
        public string status { get; set; }
        public string tagline { get; set; }
        public string title { get; set; }
        public bool video { get; set; }
        public float vote_average { get; set; }
        public int vote_count { get; set; }

        public class Genre
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public class Production_Companies
        {
            public string name { get; set; }
            public int id { get; set; }
        }

        public class Production_Countries
        {
            public string iso_3166_1 { get; set; }
            public string name { get; set; }
        }

        public class Spoken_Languages
        {
            public string iso_639_1 { get; set; }
            public string name { get; set; }
        }
    }

    public class GenreImporter
    {
        public int GenreID { get; set; }
        public string Name { get; set; }
    }

    class Program
    {
        static void Main()
        {
            Console.WriteLine("hello world");

            // 1. INSERT INTO Movies - MovieLensMovieID and Title from CSV into db
            //http://joshclose.github.io/CsvHelper/
            //http://grouplens.org/datasets/movielens/ - last imported dump was from 1/2016
            //var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest\movies.csv"));
            //while (csv.Read())
            //{
            //    var movieLensMovieID = csv.GetField<int>(0);
            //    var title = csv.GetField<string>(1);

            //    // put into db
            //    using (var db = Util.GetOpenConnection())
            //    {
            //        string sql = "INSERT INTO Movies (Title, MovieLensMovieID) Values(@Title, @MovieLensMovieID)";
            //        var result = db.Execute(sql, new { Title = title, MovieLensMovieID = movieLensMovieID });
            //    }
            //}

            // 1.8 UPDATE Movies - genres bits into Movie table
            //var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest\movies.csv"));
            //while (csv.Read())
            //{
            //    var movieLensMovieID = csv.GetField<int>(0);
            //    var genres = csv.GetField<string>(2);

            //    // find movieID
            //    int movieID;
            //    using (var db = Util.GetOpenConnection())
            //    {
            //        string sql = "SELECT MovieID FROM Movies WHERE MovieLensMovieID = @MovieLensMovieID";
            //        movieID = db.Query<int>(sql, new { movieLensMovieID }).FirstOrDefault();
            //    }
            //    string[] strArray = genres.Split('|');
            //    foreach (var genre in strArray)
            //    {
            //        var x = genre.Trim();
            //        // Column names are exactly the same as the input genre text
            //        if (x == "(no genres listed)") x = "NoGenresListed";
            //        if (x == "Sci-Fi") x = "SciFi";
            //        if (x == "Film-Noir") x = "FilmNoir";
            //        using (var db = Util.GetOpenConnection())
            //        {
            //            string sql = "UPDATE Movies SET " + x + " = 1 WHERE MovieID = @MovieID ";
            //            db.Execute(sql, new { MovieID = movieID });
            //        }
            //    }
            //}

            // 2.UPDATE Movies tmdbid and imdbid (match on MovieLensMovieID)
            //var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest\links.csv"));
            //while (csv.Read())
            //{
            //    var movieLensMovieID = csv.GetField<int>(0);
            //    var imdbID = csv.GetField<int>(1);

            //    //sometimes there is no tmdbID
            //    int tmdbID;
            //    if (!csv.TryGetField(2, out tmdbID))
            //    {
            //        tmdbID = 0;
            //    }

            //    using (var db = Util.GetOpenConnection())
            //    {
            //        string sql = "UPDATE Movies SET IMDBID = @IMDBID, TMDBID = @TMDBID WHERE MovieLensMovieID = @MovieLensMovieID";
            //        db.Execute(sql, new { IMDBID = imdbID, TMDBID = tmdbID, MovieLensMovieID = movieLensMovieID });
            //    }
            //}

            // 3.UPDATE Movies - NumberOfRatings for each movie
            //var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest\ratings.csv"));
            //var list = new List<Rating>();
            //int i = 0;
            //while (csv.Read())
            //{
            //    i++;
            //    if (i % 1000 == 0) Console.WriteLine(i);

            //    var movieLensMovieID = csv.GetField<int>(1);
            //    var currentMovie = list.FirstOrDefault(x => x.MovieLensMovieID == movieLensMovieID);
            //    if (currentMovie != null)
            //    {
            //        currentMovie.NumberOfRatings++;
            //    }
            //    else
            //    {
            //        list.Add(new Rating { MovieLensMovieID = movieLensMovieID, NumberOfRatings = 1 });
            //    }
            //}
            //foreach (var item in list)
            //{
            //    using (var db = Util.GetOpenConnection())
            //    {
            //        string sql = "UPDATE Movies SET NumberOfRatings = @NumberOfRatings WHERE MovieLensMovieID = @MovieLensMovieID";
            //        db.Execute(sql, new { item.NumberOfRatings, item.MovieLensMovieID });
            //    }
            //}
            //List<Movie> movies;
            //using (var db = Util.GetOpenConnection())
            //{
            //    movies = db.Query<Movie>("SELECT * FROM Movies WHERE NetflixURL is not null ").ToList();
            //}

            // 3.5 UPDATE Movies - calculate the average StarRating - took 10mins for 1700 netflix movies
            //var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest\ratings.csv"));
            ////var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest-small\ratings.csv"));
            //var list = new List<Rating>();
            //int i = 0;
            //// read all into a list
            //while (csv.Read())
            //{
            //    i++;
            //    if (i % 1000 == 0) Console.WriteLine(i);

            //    var movieLensMovieID = csv.GetField<int>(1);
            //    var starRating = csv.GetField<decimal>(2); // eg can be 3.5
            //    list.Add(new Rating { MovieLensMovieID = movieLensMovieID, StarRating = starRating });
            //}

            //// Get each movieLensID from the DB 
            //var movies = new List<Movie>();
            //using (var db = Util.GetOpenConnection())
            //{
            //    string sql = "SELECT MovieLensMovieID FROM Movies WHERE netflixurl is not null";
            //    movies = db.Query<Movie>(sql).ToList();
            //}

            //i = 0;
            //Parallel.ForEach(movies, movie =>
            //{
            //    i++;
            //    if (i % 10 == 0) Console.WriteLine(i);
            //    // get all ratings of this movie from the list
            //    IEnumerable<Rating> allRatings = list.Where(x => x.MovieLensMovieID == movie.MovieLensMovieID);

            //    if (allRatings.Count() != 0)
            //    {
            //        // find average rating
            //        decimal rTotal = 0;
            //        foreach (var item in allRatings) rTotal += item.StarRating;
            //        decimal rAverage = rTotal / allRatings.Count();

            //        using (var db = Util.GetOpenConnection())
            //        {
            //            var sql = "UPDATE Movies SET StarRating = @StarRating WHERE MovieLensMovieID = @MovieLensMovieID";
            //            db.Execute(sql, new { StarRating = rAverage, movie.MovieLensMovieID });
            //        }
            //    }
            //});
            //Console.WriteLine("Done :-)");

            // 3.71 SimilarMovies - If user likes film A, what others did they rate (in order of stars)
            // eg if use likes Die Hard, what others did they rate?
            //var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest\ratings.csv")); // 45k rows then db timeout
            ////var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest-small\ratings.csv")); // 179k similar movies
            //var csvRatingsList = new List<Rating>();
            //// read all rating into a list
            //while (csv.Read())
            //{
            //    var userID = csv.GetField<int>(0);
            //    var movieLensMovieID = csv.GetField<int>(1);
            //    var starRating = csv.GetField<decimal>(2); // eg can be 3.5
            //    csvRatingsList.Add(new Rating
            //    {
            //        UserID = userID,
            //        MovieLensMovieID = movieLensMovieID,
            //        StarRating = starRating
            //    });
            //}

            //// Get each MovieID and MovieLensMovieID from the DB for netflix
            //List<Movie> netflixMovies;
            //using (var db = Util.GetOpenConnection())
            //{
            //    netflixMovies =
            //        db.Query<Movie>("SELECT MovieID, MovieLensMovieID, Title FROM Movies WHERE netflixurl is not null and MovieID >= 890")
            //            .ToList();
            //}

            //// Make new list csvRatingNetflixMovies
            //var csvRatingNetflixMovies = new List<Rating>();
            //foreach (var rating in csvRatingsList)
            //{
            //    bool flag = false;
            //    foreach (var movie in netflixMovies)
            //    {
            //        if (rating.MovieLensMovieID == movie.MovieLensMovieID)
            //        {
            //            flag = true;
            //            csvRatingNetflixMovies.Add(rating);
            //            break;
            //        }
            //    }
            //    if (flag) continue;
            //}

            //// write out to db so don't need to do above again!
            //Console.WriteLine("writing to db");
            //foreach (var csvRatingNetflixMovie in csvRatingNetflixMovies)
            //{
            //    using (var db = Util.GetOpenConnection())
            //    {
            //        db.Execute(
            //            "INSERT INTO NetflixMovies (UserID, MovieLensMovieID) VALUES (@UserID, @MovieLensMovieID)",
            //            new {csvRatingNetflixMovie.UserID, csvRatingNetflixMovie.MovieLensMovieID});
            //    }
            //}
            //Console.WriteLine("end STAGE 1 db");

            // 3.72 - a good starting point from above..process
            //List<Movie> netflixMovies;
            //using (var db = Util.GetOpenConnection())
            //{
            //    netflixMovies =
            //        db.Query<Movie>("SELECT MovieID, MovieLensMovieID, Title FROM Movies WHERE netflixurl is not null and MovieID >= 890")
            //            .ToList();
            //}

            //var csvRatingNetflixMovies = new List<Rating>();
            //// load from db shortcut
            //using (var db = Util.GetOpenConnection())
            //{
            //    csvRatingNetflixMovies = db.Query<Rating>("SELECT * FROM NetflixMovies").ToList();
            //}

            //// got to 603,890 in 5 hours before sql timeout
            //// got to 1696 (out of 1699).. 1.4m similar ratings in 8 hours.. no sql timeout
            //Console.WriteLine("main loop");
            //var i = 0;
            //Parallel.ForEach(netflixMovies, nfMovie =>
            //{
            //    i++;
            //    if (i % 1 == 0) Console.WriteLine(i);
            //    // all ratings for this movie from csvRatingsList eg Sabrina has 55 ratings (small dataset)
            //    var allRatingsForMovie =
            //        csvRatingNetflixMovies.Where(x => x.MovieLensMovieID == nfMovie.MovieLensMovieID).ToList();

            //    var similarmovies = new List<SimilarMovieDTO>();

            //    // get other movies each person rated
            //    foreach (var rating in allRatingsForMovie)
            //    {
            //        var userID = rating.UserID;
            //        // get other movies that person rated
            //        var otherMovies = csvRatingNetflixMovies.Where(x => x.UserID == userID).ToList();

            //        // dump those movies into an in memory list
            //        foreach (var otherMovie in otherMovies)
            //        {
            //            similarmovies.Add(new SimilarMovieDTO { MovieLensMovieID = otherMovie.MovieLensMovieID });
            //        }
            //    }

            //    // count up the similarMovies
            //    var newSimilarMovies = new List<SimilarMovieDTO>();
            //    foreach (var similarMovieDto in similarmovies)
            //    {
            //        var y = newSimilarMovies.FirstOrDefault(x => x.MovieLensMovieID == similarMovieDto.MovieLensMovieID);

            //        if (y != null)
            //        {
            //            if (y.MovieLensMovieID != nfMovie.MovieLensMovieID)
            //                y.NumberOfTimesRatedSimilar++;
            //        }
            //        else
            //        {
            //            newSimilarMovies.Add(new SimilarMovieDTO
            //            {
            //                MovieLensMovieID = similarMovieDto.MovieLensMovieID,
            //                NumberOfTimesRatedSimilar = 1
            //            });
            //        }
            //    }

            //    var bb = newSimilarMovies.Where(x => x.NumberOfTimesRatedSimilar > 1)
            //        .OrderByDescending(x => x.NumberOfTimesRatedSimilar)
            //        .ToList();

            //    // update MovieID as we only know MovieLensMovieID here
            //    foreach (var b in bb)
            //    {
            //        var movieLensMovieID = b.MovieLensMovieID;
            //        var movie = netflixMovies.FirstOrDefault(x => x.MovieLensMovieID == movieLensMovieID);
            //        int movieID = 0;
            //        if (movie != null)
            //        {
            //            // the recommended movie is on netflixuk
            //            movieID = movie.MovieID;
            //        }
            //        b.MovieID = movieID;
            //    }

            //    var xxx = bb.Where(x => x.MovieID != 0);

            //    // insert into SimilarMovies table where movieID != 0
            //    foreach (var x in xxx)
            //    {
            //        using (var db = Util.GetOpenConnection())
            //        {
            //            // useful to stop deadlocks
            //            System.Threading.Thread.Sleep(100);
            //            var sql =
            //                "INSERT INTO SimilarMovies (MovieID, SimilarMovieID, NumberOfTimes) VALUES (@MovieID, @SimilarMovieID, @NumberOfTimes)";
            //            db.Execute(sql,
            //                new
            //                {
            //                    nfMovie.MovieID,
            //                    SimilarMovieID = x.MovieID,
            //                    NumberOfTimes = x.NumberOfTimesRatedSimilar
            //                });
            //        }
            //    }
            //});
            //Console.WriteLine("Done :-)");

            //4.Get small poster for home page, by querying TMDB
            //var apiKey = GetTMDBApiKey();
            //foreach (var movie in movies)
            //{
            //    var url = "https://api.themoviedb.org/3/movie/" + movie.TMDBID + "?api_key=" + apiKey;
            //    var json = CallURLWithRetryAndReturnJSON(url);

            //    TMDBMovieResponse tmdbMovieResponse = null;

            //    //Home / TMDBDetail / 211329 - this failed
            //    try
            //    {
            //        tmdbMovieResponse = JsonConvert.DeserializeObject<TMDBMovieResponse>(json);
            //    }
            //    catch (Exception) { }

            //    if (tmdbMovieResponse == null) continue;

            //    using (var db = Util.GetOpenConnection())
            //    {
            //        string sql = "UPDATE Movies SET TMDBPosterPath = @TMDBPosterPath WHERE MovieLensMovieID = @MovieLensMovieID";
            //        db.Execute(sql, new { TMDBPosterPath = tmdbMovieResponse.poster_path, movie.MovieLensMovieID });
            //    }
            //    Console.WriteLine(movie.Title);
            //}

            //5.get rid of year in movie tite (actually it is useful to have the movie year... keep it in for now)
            //foreach (var movie in movies)
            //{
            //    using (var db = Util.GetOpenConnection())
            //    {
            //        var title = movie.Title.Substring(0, movie.Title.Length-7);

            //        string sql = "UPDATE Movies SET Title = @Title WHERE MovieID = @MovieID";
            //        db.Execute(sql, new { Title = title, movie.MovieID });
            //    }
            //}

            // 6. Query UNOGS getting all Netflix UK films @NetflixID,@Title,@Year,@Duration,@IMDBID
            //https://unogs-unogs-v1.p.mashape.com/api.cgi?q={query}-!{syear},{eyear}-!{snfrate},{enfrate}-!{simdbrate},{eimdbrate}-!{genreid}-!{vtype}-!{audio}-!{subtitle}-!{imdbvotes}&t=ns&cl={clist}&st=adv&ob={sortby}&p={page}&sa={andor}
            // look for p=1 at the end for page
            //var url = "https://unogs-unogs-v1.p.mashape.com/api.cgi?q={query}-!1900,2017-!0,5-!0,10-!0-!Movie-!Any-!Any-!gt100&t=ns&cl=46&st=adv&ob=Relevance&p=1&sa=and";
            //var json = CallMashableURLAndReturnJSON(url);
            //var response = JsonConvert.DeserializeObject<UNOGResponse>(json);
            //int totalRecords;
            //int.TryParse(response.COUNT, out totalRecords);

            //for (int i = 1; i <= (totalRecords / 100) + 1; i++)
            //{
            //    url = "https://unogs-unogs-v1.p.mashape.com/api.cgi?q={query}-!1900,2017-!0,5-!0,10-!0-!Movie-!Any-!Any-!gt100&t=ns&cl=46&st=adv&ob=Relevance&p=" + i + "&sa=and";
            //    json = CallMashableURLAndReturnJSON(url);
            //    response = JsonConvert.DeserializeObject<UNOGResponse>(json);

            //    foreach (var item in response.ITEMS)
            //    {
            //        var x = new UNOG();
            //        x.NetflixID = Convert.ToInt32(item[0]);
            //        x.Title = item[1];
            //        x.Year = Convert.ToInt32(item[7]);
            //        x.Duration = item[8];
            //        x.IMDBIDWithTT = item[11];
            //        x.IMDBID = Convert.ToInt32(item[11].Replace("tt", ""));
            //        x.NetflixImage = item[2].Replace("http://", "");

            //        using (var db = Util.GetOpenConnection())
            //        {
            //            var sql = "INSERT INTO [dbo].[UNOG] ([NetflixID] ,[Title] ,[Year],[Duration],[IMDBIDWithTT],IMDBID,DateImported,NetflixImage)VALUES" +
            //                         "(@NetflixID, @Title, @Year, @Duration, @IMDBIDWithTT, @IMDBID, @DateImported, @NetflixImage)";
            //            db.Execute(sql, new { x.NetflixID, x.Title, x.Year, x.Duration, x.IMDBIDWithTT, x.IMDBID, DateImported = DateTime.Now, x.NetflixImage });
            //        }
            //    }
            //}

            // 7. New movies to add from UNOG which are currently in MoviesDB
            //using (var db = Util.GetOpenConnection())
            //{
            //    var msql = @"select * from Movies m
            //                            join unog u on m.IMDBID = u.IMDBID
            //                            where u.DateImported >= @DateImported -- recently imported (-1 day)
            //                            and NetflixURL is null -- hasnt been imported before";
            //    var movies = db.Query<Movie>(msql, new { DateImported = DateTime.Now.Date.AddDays(-1)}).ToList();

            //    foreach (var movie in movies)
            //    {
            //        Console.WriteLine($"New Movie on NF currently in Movies table: {movie.Title}");
            //        // update the movies netflixid in movies, using IMDBID as lookup
            //        var unog = db.Query<UNOG>("SELECT * FROM UNOG WHERE IMDBID = @IMDBID", new { movie.IMDBID }).FirstOrDefault();

            //        var sql = @"UPDATE Movies SET NetflixURL = @NetflixID, DateImported = @DateImported, NetflixPosterPath= @NetflixImage
            //                  WHERE IMDBID = @IMDBID";
            //        db.Execute(sql, new {unog.NetflixID, unog.DateImported, unog.NetflixImage, unog.IMDBID});
            //    }
            //}

            // 7.1 DELETED movies
            //using (var db = Util.GetOpenConnection())
            //{
            //    var msql = @"select * from movies
            //                 where
            //                 IMDBID not in (select IMDBID from unog where dateimported >= @DateImported) 
            //                 and DateDeleted is null-- is hasn't been deleted already
            //                 and netflixurl is not null-- one of the netflix ones";
            //    var unogs = db.Query<UNOG>(msql, new {DateImported = DateTime.Now.Date}).ToList();

            //    foreach (var unog in unogs)
            //    {
            //        Console.WriteLine($"Deleting {unog.Title}");
            //        // update the movies netflixid in movies, using IMDBID as lookup
            //        var sql = "UPDATE Movies SET DateDeleted = @DateDeleted WHERE IMDBID = @IMDBID";
            //        db.Execute(sql, new {NetflixURL = unog.NetflixID, unog.IMDBID, DateDeleted = DateTime.Now});
            //    }
            //}

            // Outliers 
            // 8 put into Movies everything from UNOG which isn't in Movielens
            // using netflix images, and no NoGenresListed genre?
            //using (var db = Util.GetOpenConnection())
            //{
            //    var msql = @"select * from unog u
            //                    left outer join movies m on u.IMDBID = m.IMDBID
            //                    where u.dateimported >= @DateImported
            //                    and m.movieid is null -- get the ones not in Movies already
            //                    order by year desc";
            //    var unogsNotInMovies = db.Query<UNOG>(msql, new { DateImported = DateTime.Now.Date.AddDays(-1) });

            //    foreach (var unog in unogsNotInMovies)
            //    {
            //        Console.WriteLine($"New Movie added not in MovieLens: {unog.Title}");
            //        // update the movies netflixid in movies, using IMDBID as lookup
            //        var sql = @"INSERT INTO [dbo].[Movies]
            //                   ([Title]
            //                   ,[DateImported]
            //                   ,[IMDBID]
            //                   ,[NetflixURL]
            //                    ,[NetflixPosterPath]
            //                   ,[NumberOfRatings]
            //                   ,[StarRating]
            //                   ,[NoGenresListed])
            //             VALUES
            //                   (@Title
            //                   ,@DateImported
            //                   ,@IMDBID
            //                   ,@NetflixID
            //                   ,@NetflixImage
            //                   ,0
            //                   ,0
            //                   ,1)";
            //        db.Execute(sql, new { unog.Title, DateImported = DateTime.Now, unog.IMDBID, unog.NetflixID, unog.NetflixImage });
            //    }
            //}
        }

        private static string CallURLWithRetryAndReturnJSON(string url)
        {
            string json = "";
            const int maxRetries = 5;
            bool done = false;
            int attempts = 0;

            while (!done)
            {
                attempts++;
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.Accept = "application/json";
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        using (var sr = new StreamReader(response.GetResponseStream()))
                        {
                            json = sr.ReadToEnd();
                        }
                    }
                    done = true;
                }
                catch (WebException ex)
                {
                    if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                    {
                        var resp = (HttpWebResponse)ex.Response;
                        if (resp.StatusCode == (HttpStatusCode)429)
                        {
                            Console.WriteLine("rate limiter! " + attempts);
                        }
                        if (resp.StatusCode == HttpStatusCode.NotFound)
                        {
                            Console.WriteLine("404 not found " + ex.Response.ResponseUri);
                            return null;
                        }
                    }
                    if (attempts >= maxRetries) throw;
                    System.Threading.Thread.Sleep(5000);
                }
            }
            return json;
        }


        //"ITEMS": [
        //[
        //  "60010657",
        //  "Missing",
        //  "http://cdn7.nflximg.net/ipl/35312/ba5e6e9b0c5c79bd39bf382b4f9f91bb8be4c352.jpg",
        //  "When an American living in a South American country vanishes in the midst of a coup, his father learns some ugly lessons in political reality.",
        //  "60010657",
        //  "3.499912",
        //  "movie",
        //  "1982",
        //  "2h2m",
        //  "",
        //  "2016-04-13",
        //  "tt0084335"
        //],
        //[
        //  "70083109",
        //  "Welcome Home Roscoe Jenkins",

        public class UNOG
        {
            public int NetflixID { get; set; }
            public string Title { get; set; }
            public int Year { get; set; }
            public string Duration { get; set; }
            public string IMDBIDWithTT { get; set; }
            public int IMDBID { get; set; }
            public string NetflixImage { get; set; }
            public DateTime DateImported { get; set; }
        }

        private static string CallMashableURLAndReturnJSON(string url)
        {
            string json = "";
            const int maxRetries = 5;
            bool done = false;
            int attempts = 0;

            var mashapeKey = GetMashapeKey();

            while (!done)
            {
                attempts++;
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.Headers.Add("X-Mashape-Key", mashapeKey);
                    request.Accept = "application/json";
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        using (var sr = new StreamReader(response.GetResponseStream()))
                        {
                            json = sr.ReadToEnd();
                        }
                    }
                    done = true;
                }
                catch (WebException ex)
                {
                    if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                    {
                        var resp = (HttpWebResponse)ex.Response;
                        if (resp.StatusCode == (HttpStatusCode)429)
                        {
                            Console.WriteLine("rate limiter! " + attempts);
                        }
                        if (resp.StatusCode == HttpStatusCode.NotFound)
                        {
                            Console.WriteLine("404 not found " + ex.Response.ResponseUri);
                            return null;
                        }
                    }
                    if (attempts >= maxRetries) throw;
                    System.Threading.Thread.Sleep(5000);
                }
            }
            return json;
        }

        public class UNOGResponse
        {
            public string COUNT { get; set; }
            public string[][] ITEMS { get; set; }
        }

        private static List<AmazonInstantVideo> GetAmazon(string title)
        {
            var accessKeyId = GetAmazonAccessKeyID();
            var secretKeyid = GetAmazonSecretKey();

            const int maxRetries = 5;
            bool done = false;
            int attempts = 0;

            var list = new List<AmazonInstantVideo>();
            while (!done)
            {
                attempts++;
                try
                {
                    var binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport)
                    {
                        MaxReceivedMessageSize = int.MaxValue
                    };
                    var endPointAddress = "https://webservices.amazon.co.uk/onca/soap?Service=AWSECommerceService";
                    var amazonClient = new AWSECommerceServicePortTypeClient(binding,
                        new EndpointAddress(endPointAddress));
                    amazonClient.ChannelFactory.Endpoint.Behaviors.Add(new AmazonSigningEndpointBehavior(accessKeyId,
                        secretKeyid));

                    var request = new ItemSearchRequest
                    {
                        SearchIndex = "Video",
                        Title = title,
                        BrowseNode = "3280626031", // Prime instant video UK
                        ResponseGroup = new[] { "Medium" } // Large Medium Small
                    };

                    var itemSearch = new ItemSearch();
                    itemSearch.Request = new[] { request };
                    var amazonAccessKeyID = GetAmazonAccessKeyID();
                    itemSearch.AWSAccessKeyId = amazonAccessKeyID;
                    itemSearch.AssociateTag = "davsmov-21";

                    //504 server too busy here..after 15 requests?
                    ItemSearchResponse response = amazonClient.ItemSearch(itemSearch);

                    if (response.Items[0].Item == null) return null;

                    foreach (var item in response.Items[0].Item)
                    {
                        list.Add(new AmazonInstantVideo
                        {
                            Title = item.ItemAttributes.Title,
                            URL = item.DetailPageURL
                        });
                    }
                    done = true;
                }
                catch (ServerTooBusyException)
                {
                    if (attempts >= maxRetries) throw;
                    System.Threading.Thread.Sleep(5000);
                }
            }
            return list;
        }

        private static string GetMashapeKey()
        {
            //var apiKey = ConfigurationManager.AppSettings["secretKey"];
            //if (apiKey == "PutAmazonSecretKeyHere")
            //{
            var apiKey = File.ReadLines(@"c:\temp\MashapeKey.txt").Take(1).First();
            //}
            return apiKey;
        }

        private static string GetAmazonSecretKey()
        {
            var apiKey = ConfigurationManager.AppSettings["secretKey"];
            if (apiKey == "PutAmazonSecretKeyHere")
            {
                apiKey = File.ReadLines(@"c:\temp\AmazonSecretKey.txt").Take(1).First();
            }
            return apiKey;
        }

        private static string GetAmazonAccessKeyID()
        {
            var apiKey = ConfigurationManager.AppSettings["accessKeyId"];
            if (apiKey == "PutAmazonAccessKeyIdHere")
            {
                apiKey = File.ReadLines(@"c:\temp\AmazonAccessKeyId.txt").Take(1).First();
            }
            return apiKey;
        }

        // Amazon Instant Video
        public class AmazonInstantVideo
        {
            public string Title { get; set; }
            public string URL { get; set; }
        }

        private static string GetTMDBApiKey()
        {
            return File.ReadLines(@"c:\temp\TMDBAPIKey.txt").Take(1).First();
        }

        class Rating
        {
            public int UserID { get; set; }
            public int MovieLensMovieID { get; set; }
            public int NumberOfRatings { get; set; }
            public decimal StarRating { get; set; }
        }
    }

    public class SimilarMovieDTO
    {
        public int MovieID { get; set; }
        public int MovieLensMovieID { get; set; }
        public int NumberOfTimesRatedSimilar { get; set; }
    }
}
// 1.5 Genres table create - 20 on 30th June 2016. **OLD NOT USED as now using 20 columns in Movie table (it is fast to query!)
//var list = new List<string>();
//var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest\movies.csv"));
//while (csv.Read())
//{
//    var movieLensMovieID = csv.GetField<int>(0);
//    var title = csv.GetField<string>(1);
//    var genres = csv.GetField<string>(2);
//    string[] strArray = genres.Split('|');
//    foreach (var genre in strArray)
//    {
//        var x = genre.Trim();
//        if (!list.Contains(x)) list.Add(x);
//    }
//}
//foreach (var genre in list)
//{
//    using (var db = Util.GetOpenConnection())
//    {
//        string sql = "INSERT INTO Genres (Name) Values(@Name)";
//        db.Execute(sql, new { Name = genre });
//    }
//}

// 1.7 MovieGenre table **OLD NOT USED - as above
//List<GenreImporter> list;
//using (var db = Util.GetOpenConnection())
//{
//    list = db.Query<GenreImporter>("SELECT * FROM Genres").ToList();
//}
//var csv = new CsvReader(new StreamReader(@"c:\temp\ml-latest\movies.csv"));
//while (csv.Read())
//{
//    var movieLensMovieID = csv.GetField<int>(0);
//    var genres = csv.GetField<string>(2);

//    // find movieID
//    int movieID;
//    using (var db = Util.GetOpenConnection())
//    {
//        string sql = "SELECT MovieID FROM Movies WHERE MovieLensMovieID = @MovieLensMovieID";
//        movieID = db.Query<int>(sql, new {movieLensMovieID }).FirstOrDefault();
//    }

//    string[] strArray = genres.Split('|');
//    foreach (var genre in strArray)
//    {
//        var g = genre.Trim(); // in case any white space
//        var genreID = list.Find(x => x.Name == g).GenreID;
//        using (var db = Util.GetOpenConnection())
//        {
//            string sql = "INSERT INTO MovieGenre (MovieID, GenreID) Values(@MovieID, @GenreID)";
//            db.Execute(sql, new { MovieID = movieID, GenreID = genreID });
//        }
//    }
//}
