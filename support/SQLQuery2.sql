select p.* --, m.Title as OrigTitle, p.Title as SimilarTitle from similarmovies s
from similarmovies s
inner join movies m on m.MovieID = s.MovieID -- orig movie
inner join movies p on p.MovieID = s.SimilarMovieID
--where m.MovieID = 31466 -- hackers
where m.MovieID = 37650 -- the unforgiven
and m.Adventure = p.Adventure
and m.Animation = p.Animation
and m.Children = p.Children
and m.Comedy = p.Comedy
and m.Fantasy = p.Fantasy
and m.Romance = p.Romance
and m.Drama = p.Drama
and m.Action = p.Action
and m.Crime = p.Crime
and m.Thriller= p.Thriller
and m.Horror = p.Horror
and m.Mystery = p.Mystery
and m.SciFi = p.SciFi
and m.IMAX = p.IMAX
and m.Documentary = p.Documentary
and m.War = p.War
and m.Musical = p.Musical
and m.Western = p.Western
and m.FilmNoir = p.FilmNoir
and m.NoGenresListed = p.NoGenresListed
order by NumberOfTimes desc

--OFFSET 10 ROWS
--FETCH NEXT 6 ROWS ONLY


--select s.*, m.Title as OrigTitle, p.Title as SimilarTitle from similarmovies s
--inner join movies m on m.MovieID = s.MovieID
--inner join movies p on p.MovieID = s.SimilarMovieID
--where m.MovieID = 37650 -- unforgiven
--order by NumberOfTimes desc

--MovieLensMovieID 1485 is liar liar
-- 19421 votes
--select n.*, m.Title from NetflixMovies n
--inner join movies m on m.MovieLensMovieID = n.MovieLensMovieID
--where n.MovieLensMovieID = 1485