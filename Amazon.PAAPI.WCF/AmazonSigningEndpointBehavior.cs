﻿using System.Configuration;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Amazon.PAAPI.WCF
{
    public class AmazonSigningEndpointBehavior : IEndpointBehavior
    {
        private string _accessKeyId = "";
        private string _secretKey = "";

        public AmazonSigningEndpointBehavior()
        {
            //this._accessKeyId = ConfigurationManager.AppSettings["accessKeyId"];
            this._accessKeyId = GetAmazonAccessKeyID();
            //this._secretKey = ConfigurationManager.AppSettings["secretKey"];
            this._secretKey = GetAmazonSecretKey();
        }

        public AmazonSigningEndpointBehavior(string accessKeyId, string secretKey)
        {
            this._accessKeyId = accessKeyId;
            this._secretKey = secretKey;
        }

        private string GetAmazonSecretKey()
        {
            string apiKey = ConfigurationManager.AppSettings["secretKey"];
            if (apiKey == "PutAmazonSecretKeyHere")
            {
                // must be local, so web.config hasn't been transformed by appveyor
                foreach (string line in System.IO.File.ReadLines(@"c:\temp\AmazonSecretKey.txt"))
                {
                    apiKey = line;
                }
            }
            return apiKey;
        }


        private string GetAmazonAccessKeyID()
        {
            string apiKey = ConfigurationManager.AppSettings["accessKeyId"];
            if (apiKey == "PutAmazonAccessKeyIdHere")
            {
                // must be local, so web.config hasn't been transformed by appveyor
                foreach (string line in System.IO.File.ReadLines(@"c:\temp\AmazonAccessKeyId.txt"))
                {
                    apiKey = line;
                }
            }
            return apiKey;
        }

        public void ApplyClientBehavior(ServiceEndpoint serviceEndpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new AmazonSigningMessageInspector(_accessKeyId, _secretKey));
        }

        public void ApplyDispatchBehavior(ServiceEndpoint serviceEndpoint, EndpointDispatcher endpointDispatcher) { return; }
        public void Validate(ServiceEndpoint serviceEndpoint) { return; }
        public void AddBindingParameters(ServiceEndpoint serviceEndpoint, BindingParameterCollection bindingParameters) { return; }
    }
}
