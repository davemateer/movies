using System;
using System.Collections.Generic;

namespace MoviesWeb.Models
{
    public enum Genre
    {
        Adventure,
        Animation,
        Children,
        Comedy,
        Fantasy,
        Romance,
        Drama,
        Action,
        Crime,
        Thriller,
        Horror,
        Mystery,
        SciFi,
        IMAX,
        Documentary,
        War,
        Musical,
        Western,
        FilmNoir,
        NoGenresListed
    }

    public class Movie
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public DateTime DateImported { get; set; }
        public string TMDBPosterPath { get; set; }
        public int MovieLensMovieID { get; set; }
        public int TMDBID { get; set; }
        public int IMDBID { get; set; }
        //public string AmazonPrimeInstantURL { get; set; }
        public string NetflixURL { get; set; }
        public string ITunesURL { get; set; }
        public int NumberOfRatings { get; set; }
        public bool Adventure { get; set; }
        public bool Animation { get; set; }
        public bool Children { get; set; }
        public bool Comedy { get; set; }
        public bool Fantasy { get; set; }
        public bool Romance { get; set; }
        public bool Drama { get; set; }
        public bool Action { get; set; }
        public bool Crime { get; set; }
        public bool Thriller { get; set; }
        public bool Horror { get; set; }
        public bool Mystery { get; set; }
        public bool SciFi { get; set; }
        public bool IMAX { get; set; }
        public bool Documentary { get; set; }
        public bool War { get; set; }
        public bool Musical { get; set; }
        public bool Western { get; set; }
        public bool FilmNoir { get; set; }
        public bool NoGenresListed { get; set; }

        public bool OnWatchList { get; set; }
        public bool OnStarredList { get; set; }

        public decimal StarRating { get; set; }

        public string NetflixPosterPath { get; set; }
    }

    //public class GenreResult
    //{
    //    public int MovieID { get; set; }
    //    public int GenreID { get; set; }
    //    public string Name { get; set; }
    //}

    //public class MovieVM
    //{
    //    public int MovieID { get; set; }
    //    public string Title { get; set; }
    //    public string TMDBPosterPath { get; set; }
    //    public int MovieLensMovieID { get; set; }
    //    public int TMDBID { get; set; }
    //    public int IMDBID { get; set; }
    //    public string AmazonPrimeInstantURL { get; set; }
    //    public string NetflixURL { get; set; }
    //    public string ITunesURL { get; set; }
    //    public int NumberOfRatings { get; set; }
    //    public IEnumerable<Genre> Genres { get; set; }
    //}

    //public class Genre
    //{
    //    public int GenreID { get; set; }
    //    public string Name { get; set; }
    //}
}