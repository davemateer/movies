﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using MoviesWeb.Controllers;
using MoviesWeb.Services;

namespace MoviesWeb
{
    // http://stackoverflow.com/a/32033303/26086
    public class MoviesControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == typeof(HomeController))
            {
                var repo = new MovieRepository();
                return new HomeController(repo);
            }

            // Other controllers will still go the normal tightly coupled way
            return base.GetControllerInstance(requestContext, controllerType);
        }
    }
}