﻿using StackExchange.Profiling;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MoviesWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Patch in our own Factory/Composition Root into MVC pipeline
            ControllerBuilder.Current.SetControllerFactory(new MoviesControllerFactory());

            //http://stackoverflow.com/questions/2246251/how-do-i-improve-asp-net-mvc-application-performance
            //ViewEngines.Engines.Clear();
            //ViewEngines.Engines.Add(new RazorViewEngine());
        }

        protected void Application_BeginRequest()
        {
            MiniProfiler.Start();
        }

        protected void Application_EndRequest()
        {
            MiniProfiler.Stop();
        }
    }
}
