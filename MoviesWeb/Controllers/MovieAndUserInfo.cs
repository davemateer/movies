namespace MoviesWeb.Controllers
{
    public class MovieAndUserInfo
    {
        public string MovieID { get; set; }
        public string UserID { get; set; }
    }
}