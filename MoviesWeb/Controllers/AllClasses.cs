using System;

namespace MoviesWeb.Controllers
{
    // Amazon Instant Video
    public class AmazonInstantVideo
    {
        public string Title { get; set; }
        public string URL { get; set; }
    }


    // ITunes Movie results query (from a movie name)
    public class ITunesResult
    {
        public int resultCount { get; set; }
        public Result[] results { get; set; }
    }

    public class Result
    {
        public string wrapperType { get; set; }
        public string kind { get; set; }
        public int collectionId { get; set; }
        public int trackId { get; set; }
        public string artistName { get; set; }
        public string collectionName { get; set; }
        public string trackName { get; set; }
        public string collectionCensoredName { get; set; }
        public string trackCensoredName { get; set; }
        public int collectionArtistId { get; set; }
        public string collectionArtistViewUrl { get; set; }
        public string collectionViewUrl { get; set; }
        public string trackViewUrl { get; set; }
        public string previewUrl { get; set; }
        public string artworkUrl30 { get; set; }
        public string artworkUrl60 { get; set; }
        public string artworkUrl100 { get; set; }
        public float collectionPrice { get; set; }
        public float trackPrice { get; set; }
        public float trackRentalPrice { get; set; }
        public float collectionHdPrice { get; set; }
        public float trackHdPrice { get; set; }
        public float trackHdRentalPrice { get; set; }
        public DateTime releaseDate { get; set; }
        public string collectionExplicitness { get; set; }
        public string trackExplicitness { get; set; }
        public int discCount { get; set; }
        public int discNumber { get; set; }
        public int trackCount { get; set; }
        public int trackNumber { get; set; }
        public int trackTimeMillis { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string primaryGenreName { get; set; }
        public string contentAdvisoryRating { get; set; }
        public string longDescription { get; set; }
    }



    //TMDB Person Movie Credits
    public class TMDBPersonMovieCredits
    {
        public Cast2[] cast { get; set; }
        public Crew2[] crew { get; set; }
        public int id { get; set; }
    }

    public class Cast2
    {
        public bool adult { get; set; }
        public string character { get; set; }
        public string credit_id { get; set; }
        public int id { get; set; }
        public string original_title { get; set; }
        public string poster_path { get; set; }
        public string release_date { get; set; }
        public string title { get; set; }
    }

    public class Crew2
    {
        public bool adult { get; set; }
        public string credit_id { get; set; }
        public string department { get; set; }
        public int id { get; set; }
        public string job { get; set; }
        public string original_title { get; set; }
        public string poster_path { get; set; }
        public string release_date { get; set; }
        public string title { get; set; }
    }



    // TMDB Search Person
    public class TMDBSearchPerson
    {
        public bool adult { get; set; }
        public object[] also_known_as { get; set; }
        public string biography { get; set; }
        public string birthday { get; set; }
        public string deathday { get; set; }
        public string homepage { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string place_of_birth { get; set; }
        public string profile_path { get; set; }
    }




    // TMDB Credits (Cast and Crew)
    public class TMDBCredits
    {
        public int id { get; set; }
        public Cast[] cast { get; set; }
        public Crew[] crew { get; set; }
    }

    public class Cast
    {
        public int cast_id { get; set; }
        public string character { get; set; }
        public string credit_id { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int order { get; set; }
        public string profile_path { get; set; }
    }

    public class Crew
    {
        public string credit_id { get; set; }
        public string department { get; set; }
        public int id { get; set; }
        public string job { get; set; }
        public string name { get; set; }
        public string profile_path { get; set; }
    }



    // TMDB Movie Images call
    public class TMDBImages
    {
        public int id { get; set; }
        public Backdrop[] backdrops { get; set; }
        public Poster[] posters { get; set; }
    }

    public class Backdrop
    {
        public string file_path { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public object iso_639_1 { get; set; }
        public float aspect_ratio { get; set; }
        public float vote_average { get; set; }
        public int vote_count { get; set; }
    }

    public class Poster
    {
        public string file_path { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string iso_639_1 { get; set; }
        public float aspect_ratio { get; set; }
        public float vote_average { get; set; }
        public int vote_count { get; set; }
    }

    // TMDBSearch 
    public class TMDBSearch
    {
        public int page { get; set; }
        public TMDBSearchResult[] results { get; set; }
        public int total_results { get; set; }
        public int total_pages { get; set; }
    }

    public class TMDBSearchResult
    {
        public string poster_path { get; set; }
        public bool adult { get; set; }
        public string overview { get; set; }
        public string release_date { get; set; }
        public int?[] genre_ids { get; set; }
        public int id { get; set; }
        public string original_title { get; set; }
        public string original_language { get; set; }
        public string title { get; set; }
        public string backdrop_path { get; set; }
        public float popularity { get; set; }
        public int vote_count { get; set; }
        public bool video { get; set; }
        public float vote_average { get; set; }
    }

    // TMDB Movie 
    public class TMDBMovieResponse
    {
        public bool adult { get; set; }
        public string backdrop_path { get; set; }
        public object belongs_to_collection { get; set; }
        public int budget { get; set; }
        public Genre[] genres { get; set; }
        public string homepage { get; set; }
        public int id { get; set; }
        public string imdb_id { get; set; }
        public string original_language { get; set; }
        public string original_title { get; set; }
        public string overview { get; set; }
        public float popularity { get; set; }
        public string poster_path { get; set; }
        public Production_Companies[] production_companies { get; set; }
        public Production_Countries[] production_countries { get; set; }
        public string release_date { get; set; }
        public int revenue { get; set; }
        public int runtime { get; set; }
        public Spoken_Languages[] spoken_languages { get; set; }
        public string status { get; set; }
        public string tagline { get; set; }
        public string title { get; set; }
        public bool video { get; set; }
        public float vote_average { get; set; }
        public int vote_count { get; set; }

        public class Genre
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public class Production_Companies
        {
            public string name { get; set; }
            public int id { get; set; }
        }

        public class Production_Countries
        {
            public string iso_3166_1 { get; set; }
            public string name { get; set; }
        }

        public class Spoken_Languages
        {
            public string iso_639_1 { get; set; }
            public string name { get; set; }
        }
    }

    

    // OMDB Search
    public class MovieResponseSearchRoot
    {
        public MovieResponseSearch[] Search { get; set; }
        public string totalResults { get; set; }
        public string Response { get; set; }
    }

    public class MovieResponseSearch
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string imdbID { get; set; }
        public string Type { get; set; }
        public string Poster { get; set; }
    }

    public class MovieResponse
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Rated { get; set; }
        public string Released { get; set; }
        public string Runtime { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Writer { get; set; }
        public string Actors { get; set; }
        public string Plot { get; set; }
        public string PlotShort { get; set; }
        public string PlotFull { get; set; }
        public string Language { get; set; }
        public string Country { get; set; }
        public string Awards { get; set; }
        public string Poster { get; set; }
        public string Metascore { get; set; }
        public string imdbRating { get; set; }
        public string imdbVotes { get; set; }
        public string imdbID { get; set; }
        public string Type { get; set; }
        public string Response { get; set; }
    }


    public class MovieResponseTomato
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Rated { get; set; }
        public string Released { get; set; }
        public string Runtime { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Writer { get; set; }
        public string Actors { get; set; }
        public string Plot { get; set; }
        public string PlotShort { get; set; }
        public string PlotFull { get; set; }
        public string Language { get; set; }
        public string Country { get; set; }
        public string Awards { get; set; }
        public string Poster { get; set; }
        public string Metascore { get; set; }
        public string imdbRating { get; set; }
        public string imdbVotes { get; set; }
        public string imdbID { get; set; }
        public string Type { get; set; }
        public string tomatoMeter { get; set; }
        public string tomatoImage { get; set; }
        public string tomatoRating { get; set; }
        public string tomatoReviews { get; set; }
        public string tomatoFresh { get; set; }
        public string tomatoRotten { get; set; }
        public string tomatoConsensus { get; set; }
        public string tomatoUserMeter { get; set; }
        public string tomatoUserRating { get; set; }
        public string tomatoUserReviews { get; set; }
        public string tomatoURL { get; set; }
        public string DVD { get; set; }
        public string BoxOffice { get; set; }
        public string Production { get; set; }
        public string Website { get; set; }
        public string Response { get; set; }
    }

}