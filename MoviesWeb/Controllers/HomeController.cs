﻿using Microsoft.AspNet.Identity;
using MoviesWeb.Models;
using MoviesWeb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.UI;

namespace MoviesWeb.Controllers
{
    public class HomeController : Controller
    {
        MovieRepository repo;

        public HomeController(MovieRepository repo)
        {
            this.repo = repo;
        }

        [OutputCache(Duration = 60, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult LoginPartial()
        {
            return PartialView("_LoginPartial");
        }

        // Child action - Helper method that _layout uses
        [OutputCache(Duration = 60, VaryByParam = "none")]
        public ActionResult GetLinkerTime()
        {
            var linkTimeLocal = Assembly.GetExecutingAssembly().GetLinkerTime();
            return Content(linkTimeLocal);
        }

        [HttpPost]
        public ActionResult Contact(string message)
        {
            repo.SaveContact(message);
            ViewBag.Message = message;
            return View();
        }

        public ActionResult Index(string[] g = null, int p = 1, string wl = "off", string starred = "off",
            string submitButton = "", string searchTerm = "", string submitButtonLastValue = "",
            string numberPerPage = "100")
        {
            // g is genre
            // p is page
            // wl is watchlist
            bool watchListSelected = wl == "on";
            bool starredListSelected = starred == "on";
            bool searchTermSelected = searchTerm != "";

            // If user is logged in, get their watchList and starredLists
            int userID;
            var watchList = new List<int>();
            var starredList = new List<int>();
            if (int.TryParse(User.Identity.GetUserId(), out userID))
            {
                watchList = repo.GetMoviesInUsersWatchList(userID).ToList();
                starredList = repo.GetMoviesInUsersStarredList(userID).ToList();
            }

            // If the user has pressed on a new page, respect sort order
            if (submitButtonLastValue != "" && submitButton=="")
            {
                submitButton = submitButtonLastValue;
            }

            // Sort Order button Defaults
            string sortOrder = "";
            if (submitButton == "")
            {
                sortOrder = "NumberOfRatings Desc";
                ViewBag.OrderByPopularity = "Order By Popularity";
                ViewBag.OrderByStarRating = "Order By StarRating";
                ViewBag.OrderByMostRecentlyAdded = "Order By Most Recently Added";
            }

            var cont = true;
            if (submitButton == "Order By Popularity")
            {
                sortOrder = "NumberOfRatings Desc";
                ViewBag.OrderByPopularity = "Order By Popularity Desc";
                cont = false;
            }
            if (submitButton == "Order By Popularity Desc" && cont)
            {
                sortOrder = "NumberOfRatings";
                ViewBag.OrderByPopularity = "Order By Popularity";
                cont = false;
            }
            if (submitButton == "Order By StarRating" && cont)
            {
                sortOrder = "StarRating desc";
                ViewBag.OrderByStarRating = "Order By StarRating Desc";
                cont = false;
            }
            if (submitButton == "Order By StarRating Desc" && cont)
            {
                sortOrder = "StarRating";
                ViewBag.OrderByStarRating = "Order By StarRating";
                cont = false;
            }
            if (submitButton == "Order By Most Recently Added" && cont)
            {
                sortOrder = "DateImported Desc";
                ViewBag.OrderByMostRecentlyAdded = "Order By Most Recently Added Desc";
                cont = false;
            }
            if (submitButton == "Order By Most Recently Added Desc" && cont)
            {
                sortOrder = "DateImported";
                ViewBag.OrderByMostRecentlyAdded = "Order By Most Recently Added";
            }

            // Set other buttons back to their defaults (as can only order 1 at a time)
            if (ViewBag.OrderByPopularity == null) ViewBag.OrderByPopularity = "Order By Popularity";
            if (ViewBag.OrderByStarRating == null) ViewBag.OrderByStarRating = "Order By StarRating";
            if (ViewBag.OrderByMostRecentlyAdded == null)
                ViewBag.OrderByMostRecentlyAdded = "Order By Most Recently Added";

            int n;
            int.TryParse(numberPerPage, out n);

            // Search - ignore genre filters, but respect sort order
            cont = true;
            IEnumerable<Movie> movies = null;
            if (searchTermSelected)
            {
                movies = repo.GetMovies(p, sortOrder: sortOrder, searchTerm: searchTerm, numberPerPage: n);
                cont = false;
            }

            // Any genre boxes ticked?
            var genres = new List<Genre>();
            if (g != null && cont)
            {
                // passing only the filter boxes that are checked
                foreach (var genre in g)
                {
                    Genre x;
                    Enum.TryParse(genre, out x);
                    genres.Add(x);
                }
                movies = repo.GetMovies(p, genres, watchListSelected, userID, starredListSelected, 
                    sortOrder, numberPerPage: n);
                cont = false;
            }

            // No genres are ticked
            if (cont)
            {
                movies = repo.GetMovies(p, isWatchListSelected: watchListSelected, userID: userID,
                    IsStarredListSelected: starredListSelected, sortOrder: sortOrder, numberPerPage: n);
            }

            // Set any in watchlist and starredlist
            foreach (var movie in movies)
            {
                if (watchList.Contains(movie.MovieID)) movie.OnWatchList = true;
                if (starredList.Contains(movie.MovieID)) movie.OnStarredList = true;
            }

            ViewBag.Movies = movies;

            // Get 6 Similar Movies to the first one that have just been returned
            ViewBag.SimilarMovies = repo.GetSimilarMovies(movies.FirstOrDefault());

            // Paging
            ViewBag.PreviousPage = p == 1 ? 1 : p - 1;
            ViewBag.NextPage = p + 1;

            // Pass back the tick boxes which are ticked
            ViewBag.Genres = g;
            ViewBag.WL = watchListSelected;
            ViewBag.IsStarredListSelected = starredListSelected;

            // Search term sticky
            ViewBag.SearchTerm = searchTerm;

            // Sort order sticky (for paging)
            ViewBag.SubmitButtonLastValue = submitButton;
            // Number per page drop down sticky
            ViewBag.NumberPerPage = numberPerPage;
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddMovieToWatchList(string movieID)
        {
            int movieIDInt;
            if (!int.TryParse(movieID, out movieIDInt)) return null;

            int userID = int.Parse(User.Identity.GetUserId());

            repo.SaveMovieToWatchList(movieIDInt, userID);
            // return something to know if the method has succeeded ie if 401,then movieID will be undefined
            return Json(new { movieID });
        }

        [HttpPost]
        [Authorize]
        public ActionResult RecommendMovie(string movieID)
        {
            int movieIDInt;
            if (!int.TryParse(movieID, out movieIDInt)) return null;

            int userID = int.Parse(User.Identity.GetUserId());

            repo.SaveMovieAsRecommended(movieIDInt, userID);

            return Json(new { movieID });
        }

        //[HttpGet]
        //public ActionResult TMDBDetail(int id = 550)
        //{
        //    var apiKey = GetApiKey();

        //    var url = "https://api.themoviedb.org/3/movie/" + id + "?api_key=" + apiKey;
        //    var json = CallURLAndReturnJSON(url);

        //    TMDBMovieResponse tmdbMovieResponse = null;

        //    //Home / TMDBDetail / 211329 - this failed
        //    try
        //    {
        //        tmdbMovieResponse = JsonConvert.DeserializeObject<TMDBMovieResponse>(json);
        //    }
        //    catch (Exception) { }

        //    if (tmdbMovieResponse == null) tmdbMovieResponse = new TMDBMovieResponse();

        //    // more images?
        //    url = "http://api.themoviedb.org/3/movie/" + id + "/images" + "?api_key=" + apiKey;
        //    json = CallURLAndReturnJSON(url);
        //    var tmdbImages = JsonConvert.DeserializeObject<TMDBImages>(json);
        //    ViewBag.TMDBImages = tmdbImages;

        //    // cast
        //    //http://private-anon-cd04c9feb-themoviedb.apiary-mock.com/3/movie/id/credits
        //    url = "http://api.themoviedb.org/3/movie/" + id + "/credits" + "?api_key=" + apiKey;
        //    json = CallURLAndReturnJSON(url);
        //    var tmdbCredits = JsonConvert.DeserializeObject<TMDBCredits>(json);
        //    ViewBag.TMDBCredits = tmdbCredits;

        //    // itunes lookup
        //    //https://itunes.apple.com/search?term=pulp+fiction&entity=movie&country=gb
        //    url = "https://itunes.apple.com/search?term=" + tmdbMovieResponse.title + "&entity=movie&country=gb";
        //    json = CallURLAndReturnJSON(url);
        //    var itunes = JsonConvert.DeserializeObject<ITunesResult>(json);
        //    ViewBag.ITunes = itunes;

        //    // amazon instant video lookup
        //    List<AmazonInstantVideo> list = GetAmazon(tmdbMovieResponse.title);
        //    ViewBag.ListAmazonInstantVideo = list;

        //    return View(tmdbMovieResponse);
        //}

        //[HttpGet]
        //public ActionResult TMDBSearchPerson(int personID = 62)
        //{
        //    var apiKey = GetApiKey();

        //    var url = "http://api.themoviedb.org/3/person/" + personID + "?api_key=" + apiKey;
        //    var json = CallURLAndReturnJSON(url);

        //    var tmdbSearchPerson = JsonConvert.DeserializeObject<TMDBSearchPerson>(json);

        //    // Person movies they have been in
        //    url = "http://api.themoviedb.org/3/person/" + personID + "/movie_credits?api_key=" + apiKey;
        //    json = CallURLAndReturnJSON(url);

        //    var tmdbPersonMovieCredits = JsonConvert.DeserializeObject<TMDBPersonMovieCredits>(json);
        //    ViewBag.TMDBPersonMovieCredits = tmdbPersonMovieCredits;

        //    return View(tmdbSearchPerson);
        //}

        //private string GetApiKey()
        //{
        //    var apiKey = ConfigurationManager.AppSettings["TMDBAPIKey"];
        //    if (apiKey == "PutSecretKeyHere")
        //    {
        //        apiKey = System.IO.File.ReadLines(@"c:\temp\TMDBAPIKey.txt").Take(1).First();
        //    }
        //    return apiKey;
        //}

        //private string CallURLAndReturnJSON(string url)
        //{
        //    string json = "";
        //    string urlMiniProfiler = url;

        //    // strip out the secret API key for miniprofiler
        //    if (url.Contains("api_key="))
        //    {
        //        var startingIndex = url.IndexOf("api_key=");
        //        urlMiniProfiler = url.Substring(0, startingIndex);
        //        urlMiniProfiler = urlMiniProfiler + "api_key=Secret";
        //    }

        //    using (mp.CustomTiming("http", urlMiniProfiler))
        //    {
        //        var request = (HttpWebRequest)WebRequest.Create(url);
        //        request.Accept = "application/json";
        //        try
        //        {
        //            var response = (HttpWebResponse)request.GetResponse();
        //            using (var sr = new StreamReader(response.GetResponseStream()))
        //            {
        //                json = sr.ReadToEnd();
        //            }
        //        }
        //        catch
        //        {
        //            // swallow and return nothing
        //        }
        //    }
        //    return json;
        //}

        //private List<AmazonInstantVideo> GetAmazon(string title)
        //{
        //    var accessKeyId = GetAmazonAccessKeyID();
        //    var secretKeyid = GetAmazonSecretKey();

        //    var binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport) { MaxReceivedMessageSize = int.MaxValue };
        //    var endPointAddress = "https://webservices.amazon.co.uk/onca/soap?Service=AWSECommerceService";
        //    var amazonClient = new AWSECommerceServicePortTypeClient(binding, new EndpointAddress(endPointAddress));
        //    amazonClient.ChannelFactory.Endpoint.Behaviors.Add(new AmazonSigningEndpointBehavior(accessKeyId, secretKeyid));

        //    var request = new ItemSearchRequest
        //    {
        //        SearchIndex = "Video",
        //        Title = title,
        //        BrowseNode = "3280626031", // Prime instant video UK
        //        ResponseGroup = new[] { "Medium" } // Large Medium Small
        //    };

        //    var itemSearch = new ItemSearch();
        //    itemSearch.Request = new[] { request };
        //    var amazonAccessKeyID = GetAmazonAccessKeyID();
        //    itemSearch.AWSAccessKeyId = amazonAccessKeyID;
        //    itemSearch.AssociateTag = "davsmov-21";

        //    ItemSearchResponse response = amazonClient.ItemSearch(itemSearch);

        //    var list = new List<AmazonInstantVideo>();
        //    if (response.Items[0].Item == null) return null;

        //    foreach (var item in response.Items[0].Item)
        //    {
        //        list.Add(new AmazonInstantVideo
        //        {
        //            Title = item.ItemAttributes.Title,
        //            URL = item.DetailPageURL
        //        });
        //    }
        //    return list;
        //}

        //private string GetAmazonSecretKey()
        //{
        //    var apiKey = ConfigurationManager.AppSettings["secretKey"];
        //    if (apiKey == "PutAmazonSecretKeyHere")
        //    {
        //        apiKey = System.IO.File.ReadLines(@"c:\temp\AmazonSecretKey.txt").Take(1).First();
        //    }
        //    return apiKey;
        //}

        //private string GetAmazonAccessKeyID()
        //{
        //    var apiKey = ConfigurationManager.AppSettings["accessKeyId"];
        //    if (apiKey == "PutAmazonAccessKeyIdHere")
        //    {
        //        apiKey = System.IO.File.ReadLines(@"c:\temp\AmazonAccessKeyId.txt").Take(1).First();
        //    }
        //    return apiKey;
        //}

        //[Authorize]
       


    }
}

//[HttpGet]
//public ActionResult OMDB(string searchTerm = "die hard")
//{
//    // short
//    var url = "http://www.omdbapi.com/?t=" + searchTerm + "&y=&plot=short&r=json&tomatoes=true";
//    var json = CallURLAndReturnJSON(url);

//    ViewBag.SearchTerm = searchTerm;
//    ViewBag.URL = url;
//    var movieResponse = JsonConvert.DeserializeObject<MovieResponseTomato>(json);
//    movieResponse.PlotShort = movieResponse.Plot;

//    // full
//    url = "http://www.omdbapi.com/?t=" + searchTerm + "&y=&plot=full&r=json&tomatoes=true";
//    json = CallURLAndReturnJSON(url);

//    var movieResponseFull = JsonConvert.DeserializeObject<MovieResponseTomato>(json);
//    movieResponse.PlotFull = movieResponseFull.Plot;

//    // serach
//    // http://www.omdbapi.com/?s=die%20hard&y=&plot=short&r=json
//    url = "http://www.omdbapi.com/?s=" + searchTerm + "&y=&r=json";
//    json = CallURLAndReturnJSON(url);

//    var movieResponseSearchRoot = JsonConvert.DeserializeObject<MovieResponseSearchRoot>(json);
//    ViewBag.MRSR = movieResponseSearchRoot;
//    ViewBag.URLSearch = url;

//    // themoviedb api call
//    // don't want secret API key in public repo
//    // save in appveyor as environment variable 
//    // which should get written to the web.config

//    var apiKey = GetApiKey();

//    url = "https://api.themoviedb.org/3/movie/550?api_key=" + apiKey;
//    json = CallURLAndReturnJSON(url);

//    var tmdbResponse = JsonConvert.DeserializeObject<TMDBMovieResponse>(json);
//    ViewBag.TMDBResponse = tmdbResponse;
//    ViewBag.URLTMDB = url;

//    ViewBag.TMDBApiKey = apiKey;
//    return View(movieResponse);
//}

//TMDBSearch tmdbSearch = null;
//if (searchTerm != "")
//{
//    // the user has pressed serach   
//    var apiKey = GetApiKey();

//    var url = "http://api.themoviedb.org/3/search/movie" + "?query=" + searchTerm + "&api_key=" + apiKey;
//    var json = CallURLAndReturnJSON(url);

//    tmdbSearch = JsonConvert.DeserializeObject<TMDBSearch>(json);
//}


//[OutputCache(Duration = 60, VaryByParam = "none")]
//public ActionResult GetLinkerTime()
//{
//    var cache = HttpRuntime.Cache;
//    string linkTimeLocal;
//    var linkTimeLocalFromCache = cache["linkTimeLocal"];
//    if (linkTimeLocalFromCache == null)
//    {
//        cache.Add("linkTimeLocal", Assembly.GetExecutingAssembly().GetLinkerTime(), null, DateTime.Now.AddDays(1), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
//        linkTimeLocal = (string)cache["linkTimeLocal"];
//    }
//    else linkTimeLocal = linkTimeLocalFromCache + " Cache Hit ";
//    return Content(linkTimeLocal);
//}