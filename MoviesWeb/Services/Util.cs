using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using StackExchange.Profiling;
using StackExchange.Profiling.Data;
using StackExchange.Profiling.SqlFormatters;

namespace MoviesWeb.Services
{
    public static class Util
    {
        // Appveyor is configured to set AppveyorSQL2014ConnectionString in appveyor.yml
        // Otherwise get the value from web.config 
        static readonly string ConnectionString =
            Environment.GetEnvironmentVariable("AppveyorSQL2014ConnectionString") ??
            ConfigurationManager.ConnectionStrings["DMoviesConnectionString"].ConnectionString;

        public static IDbConnection GetOpenConnection()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            MiniProfiler.Settings.SqlFormatter = new SqlServerFormatter();
            return new ProfiledDbConnection(connection, MiniProfiler.Current);
        }
    }
}