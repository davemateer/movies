using MoviesWeb.Models;
using StackExchange.Profiling.Helpers.Dapper;
using System.Collections.Generic;

namespace MoviesWeb.Services
{
    public class MovieRepository
    {
        public IEnumerable<Movie> GetMovies(int page, List<Genre> genresToShow = null, bool isWatchListSelected = false, int userID = 0,
            bool IsStarredListSelected = false, string sortOrder = "", string searchTerm = "", int numberPerPage = 6)
        {
            var offset = (page - 1) * numberPerPage;
            using (var db = Util.GetOpenConnection())
            {
                var sql = "SELECT * FROM Movies m ";

                if (isWatchListSelected)
                {
                    sql += "INNER JOIN WatchList w ON w.MovieID = m.MovieID AND w.UserID = @UserID ";
                }
                if (IsStarredListSelected)
                {
                    sql += "INNER JOIN StarredList s ON s.MovieID = m.MovieID AND s.UserID = @UserID ";
                }

                sql += @"WHERE NetflixURL IS NOT NULL
                         AND DateDeleted IS NULL ";

                if (searchTerm != "")
                {
                    sql += "AND Title like '%' + @SearchTerm + '%' ";
                }

                if (genresToShow != null)
                {
                    sql += " AND (";
                    // any of the terms can be 1.  eg if a War and Comedy film, and only Comedy is ticked, then return it
                    int i = 0;
                    foreach (var genre in genresToShow)
                    {
                        if (i == 0) sql += $"{genre} = 1 ";
                        else sql += $"OR {genre} = 1 ";
                        i++;
                    }
                    sql += ") ";
                }
                if (sortOrder == "") sortOrder = "NumberOfRatings";
                sql += $"ORDER BY {sortOrder} ";
                sql += $@"
                OFFSET {offset} ROWS 
                FETCH NEXT {numberPerPage} ROWS ONLY ";

                return db.Query<Movie>(sql, new { userID, searchTerm });
            }
        }

        public IEnumerable<Movie> GetSimilarMovies(Movie movie)
        {
            if (movie == null) return null;
            using (var db = Util.GetOpenConnection())
            {
                var sql = $@"
                    select p.*
                    from similarmovies s
                    inner join movies m on m.MovieID = s.MovieID
                    inner join movies p on p.MovieID = s.SimilarMovieID
                    where m.MovieLensMovieID = {movie.MovieLensMovieID} 
                    and m.Adventure = p.Adventure
                    and m.Animation = p.Animation
                    and m.Children = p.Children
                    and m.Comedy = p.Comedy
                    and m.Fantasy = p.Fantasy
                    and m.Romance = p.Romance
                    and m.Drama = p.Drama
                    and m.Action = p.Action
                    and m.Crime = p.Crime
                    and m.Thriller= p.Thriller
                    and m.Horror = p.Horror
                    and m.Mystery = p.Mystery
                    and m.SciFi = p.SciFi
                    and m.IMAX = p.IMAX
                    and m.Documentary = p.Documentary
                    and m.War = p.War
                    and m.Musical = p.Musical
                    and m.Western = p.Western
                    and m.FilmNoir = p.FilmNoir
                    and m.NoGenresListed = p.NoGenresListed
                    and m.DateDeleted is null
                    and p.DateDeleted is null
                    order by NumberOfTimes desc
                    ";
                return db.Query<Movie>(sql);
            }
        }

        public void SaveMovieToWatchList(int movieID, int userID)
        {
            using (var db = Util.GetOpenConnection())
            {
                // If movie exists in persons watchlist already, delete it
                // http://stackoverflow.com/questions/21208719/update-if-exists-else-insert-in-sql-server-2008
                var sql = @"
                    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
                    BEGIN TRANSACTION;
                    DELETE FROM WatchList WHERE (MovieID = @MovieID AND UserID = @UserID);
                    IF @@ROWCOUNT = 0
                    BEGIN
                      INSERT INTO WatchList (MovieID, UserID) VALUES(@MovieID, @UserID)
                    END
                    COMMIT TRANSACTION;";
                db.Execute(sql, new { movieID, userID });
            }
        }

        public void SaveMovieAsRecommended(int movieID, int userID)
        {
            using (var db = Util.GetOpenConnection())
            {
                var sql = @"
                    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
                    BEGIN TRANSACTION;
                    DELETE FROM StarredList WHERE (MovieID = @MovieID AND UserID = @UserID);
                    IF @@ROWCOUNT = 0
                    BEGIN
                      INSERT INTO StarredList (MovieID, UserID) VALUES(@MovieID, @UserID)
                    END
                    COMMIT TRANSACTION;";
                db.Execute(sql, new { movieID, userID });
            }
        }

        public IEnumerable<int> GetMoviesInUsersWatchList(int userID)
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<int>("SELECT MovieID FROM Watchlist WHERE UserID = @UserID", new { userID });
            }
        }

        public IEnumerable<int> GetMoviesInUsersStarredList(int userID)
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<int>("SELECT MovieID FROM Starredlist WHERE UserID = @UserID", new { userID });
            }
        }

        public void SaveContact(string message)
        {
            using (var db = Util.GetOpenConnection())
            {
                var sql = @"INSERT INTO Test (message) VALUES (@Message)";
                db.Execute(sql, new {message });
            }
        }
    }
}