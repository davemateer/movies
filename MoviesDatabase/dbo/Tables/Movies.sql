﻿CREATE TABLE [dbo].[Movies] (
    [MovieID]           INT             IDENTITY (1, 1) NOT NULL,
    [Title]             NVARCHAR (255)  NOT NULL,
    [DateImported]      DATETIME        NULL,
    [DateDeleted]       DATETIME        NULL,
    [TMDBPosterPath]    NVARCHAR (255)  NULL,
    [NetflixPosterPath] NVARCHAR (255)  NULL,
    [MovieLensMovieID]  INT             NULL,
    [TMDBID]            INT             NULL,
    [IMDBID]            INT             NULL,
    [NetflixURL]        NVARCHAR (1024) NULL,
    [ITunesURL]         NVARCHAR (1024) NULL,
    [NumberOfRatings]   INT             NULL,
    [StarRating]        DECIMAL (3, 2)  NULL,
    [Adventure]         BIT             CONSTRAINT [DF_Movies_Adventure] DEFAULT ((0)) NOT NULL,
    [Animation]         BIT             CONSTRAINT [DF_Movies_Animation] DEFAULT ((0)) NOT NULL,
    [Children]          BIT             CONSTRAINT [DF_Movies_Children] DEFAULT ((0)) NOT NULL,
    [Comedy]            BIT             CONSTRAINT [DF_Movies_Comedy] DEFAULT ((0)) NOT NULL,
    [Fantasy]           BIT             CONSTRAINT [DF_Movies_Fantasy] DEFAULT ((0)) NOT NULL,
    [Romance]           BIT             CONSTRAINT [DF_Movies_Romance] DEFAULT ((0)) NOT NULL,
    [Drama]             BIT             CONSTRAINT [DF_Movies_Drama] DEFAULT ((0)) NOT NULL,
    [Action]            BIT             CONSTRAINT [DF_Movies_Action] DEFAULT ((0)) NOT NULL,
    [Crime]             BIT             CONSTRAINT [DF_Movies_Crime] DEFAULT ((0)) NOT NULL,
    [Thriller]          BIT             CONSTRAINT [DF_Movies_Thriller] DEFAULT ((0)) NOT NULL,
    [Horror]            BIT             CONSTRAINT [DF_Movies_Horror] DEFAULT ((0)) NOT NULL,
    [Mystery]           BIT             CONSTRAINT [DF_Movies_Mystery] DEFAULT ((0)) NOT NULL,
    [SciFi]             BIT             CONSTRAINT [DF_Movies_Sci-Fi] DEFAULT ((0)) NOT NULL,
    [IMAX]              BIT             CONSTRAINT [DF_Movies_IMAX] DEFAULT ((0)) NOT NULL,
    [Documentary]       BIT             CONSTRAINT [DF_Movies_Documentary] DEFAULT ((0)) NOT NULL,
    [War]               BIT             CONSTRAINT [DF_Movies_War] DEFAULT ((0)) NOT NULL,
    [Musical]           BIT             CONSTRAINT [DF_Movies_Musical] DEFAULT ((0)) NOT NULL,
    [Western]           BIT             CONSTRAINT [DF_Movies_Western] DEFAULT ((0)) NOT NULL,
    [FilmNoir]          BIT             CONSTRAINT [DF_Movies_Film-Noir] DEFAULT ((0)) NOT NULL,
    [NoGenresListed]    BIT             CONSTRAINT [DF_Movies_No Genres Listed] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Movies] PRIMARY KEY CLUSTERED ([MovieID] ASC)
);









