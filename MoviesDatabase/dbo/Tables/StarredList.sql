﻿CREATE TABLE [dbo].[StarredList] (
    [StarredListID] INT IDENTITY (1, 1) NOT NULL,
    [MovieID]       INT NOT NULL,
    [UserID]        INT NOT NULL,
    CONSTRAINT [PK_StarredList] PRIMARY KEY CLUSTERED ([StarredListID] ASC),
    CONSTRAINT [FK_StarredList_AspNetUsers] FOREIGN KEY ([UserID]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_StarredList_Movies] FOREIGN KEY ([MovieID]) REFERENCES [dbo].[Movies] ([MovieID])
);



