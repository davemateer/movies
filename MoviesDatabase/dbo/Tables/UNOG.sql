﻿CREATE TABLE [dbo].[UNOG] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [NetflixID]    INT            NULL,
    [Title]        NVARCHAR (255) NULL,
    [Year]         INT            NULL,
    [Duration]     NVARCHAR (255) NULL,
    [IMDBIDWithTT] NVARCHAR (50)  NULL,
    [IMDBID]       INT            NULL,
    [DateImported] DATETIME       NULL,
    [NetflixImage] NVARCHAR (255) NULL,
    CONSTRAINT [PK_UNOG] PRIMARY KEY CLUSTERED ([Id] ASC)
);





