﻿CREATE TABLE [dbo].[SimilarMovies] (
    [ID]             INT IDENTITY (1, 1) NOT NULL,
    [MovieID]        INT NOT NULL,
    [SimilarMovieID] INT NULL,
    [NumberOfTimes]  INT NOT NULL,
    CONSTRAINT [PK_SimilarMovies] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_SimilarMovies_Movies] FOREIGN KEY ([MovieID]) REFERENCES [dbo].[Movies] ([MovieID])
);



