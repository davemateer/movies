﻿CREATE TABLE [dbo].[NetflixMovies] (
    [id]               INT IDENTITY (1, 1) NOT NULL,
    [UserID]           INT NOT NULL,
    [MovieLensMovieID] INT NOT NULL,
    CONSTRAINT [PK_NetflixMovies] PRIMARY KEY CLUSTERED ([id] ASC)
);

