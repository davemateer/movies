﻿CREATE TABLE [dbo].[WatchList] (
    [WatchListID] INT IDENTITY (1, 1) NOT NULL,
    [MovieID]     INT NOT NULL,
    [UserID]      INT NOT NULL,
    CONSTRAINT [PK_WatchList] PRIMARY KEY CLUSTERED ([WatchListID] ASC),
    CONSTRAINT [FK_WatchList_AspNetUsers] FOREIGN KEY ([UserID]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_WatchList_Movies] FOREIGN KEY ([MovieID]) REFERENCES [dbo].[Movies] ([MovieID])
);



