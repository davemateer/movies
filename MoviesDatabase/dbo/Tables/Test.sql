﻿CREATE TABLE [dbo].[Test] (
    [ID]      INT            IDENTITY (1, 1) NOT NULL,
    [Message] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Test] PRIMARY KEY CLUSTERED ([ID] ASC)
);

